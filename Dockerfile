FROM node:alpine

WORKDIR /app

COPY package*.json /

RUN npm install

COPY . ./

ENV ENV_SILENT=true

CMD ["npm", "start"]


